package testcase;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import config.CommonFunctions;
import config.DataProviders;
import config.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Properties;

public class ExecuteTest {

    WebDriver webDriver = null;
    static ExtentTest test;
    static ExtentReports report;

    @BeforeTest
    public void load() {
        report = new ExtentReports(System.getProperty("user.dir") + "/iLABReport.html");
        test = report.startTest("ExecuteTest");
    }

    @Test(dataProvider = "iLabData", dataProviderClass = DataProviders.class)
    public void execute(String testCase, String keyword, String objectName, String objectType, String value) throws Exception {

        if(testCase != null && testCase.length() != 0) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/Drivers/Windows/chromedriver.exe");
            //System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/Drivers/Windows/geckodriver.exe");
            webDriver = new ChromeDriver();
            //webDriver = new FirefoxDriver();
            webDriver.manage().window().maximize();
        }

        Utils utils = new Utils();
        Properties properties = utils.loadPropertiesFile();
        CommonFunctions commonFunctions = new CommonFunctions(webDriver);

        commonFunctions.goToURL(properties, keyword, value);
        test.log(LogStatus.PASS, "Navigated to iLab Home Page successfully");

        commonFunctions.click(properties, keyword, objectName, objectType);
        test.log(LogStatus.PASS, "Element clicked successfully");

        commonFunctions.capture(properties, keyword, objectName, objectType, value);
        test.log(LogStatus.PASS, "Field captured successfully");

        commonFunctions.getText(properties, keyword, objectName, objectType);
        test.log(LogStatus.PASS, "Text got successfully");
    }

    @AfterTest
    public void close() {
        report.endTest(test);
        report.flush();
        webDriver.close();
    }
}
