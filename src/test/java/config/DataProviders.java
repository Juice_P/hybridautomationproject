package config;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.DataProvider;

import java.io.IOException;

public class DataProviders {

    @DataProvider(name = "iLabData")
    public Object[][] getDataProvider() throws IOException {
        Object[][] objects;
        ExcelReader excelReader = new ExcelReader();

        Sheet sheet = excelReader.readExcel(System.getProperty("user.dir") + "/", "TestCase.xlsx", "KeywordFramework");
        int count = sheet.getLastRowNum() - sheet.getFirstRowNum();
        objects = new Object[count][5];

        for(int x = 0; x < count; x++) {
            Row row = sheet.getRow(x + 1);

            for(int y = 0; y < row.getLastCellNum(); y++) {
                objects[x][y] = row.getCell(y).toString();
            }
        }

        return objects;
    }
}
