package config;

import java.io.*;
import java.util.Properties;

public class Utils {

    Properties properties = new Properties();

    public Properties loadPropertiesFile() throws IOException {
        InputStream inputStream = new FileInputStream(new File(System.getProperty("user.dir") + "/src/test/java/config/objects.properties"));
        properties.load(inputStream);

        return properties;
    }
}
