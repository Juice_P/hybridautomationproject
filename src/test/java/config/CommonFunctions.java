package config;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CommonFunctions {

    WebDriver webDriver;

    public CommonFunctions(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void goToURL(Properties properties, String operation, String value) throws Exception {
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        if(operation.equalsIgnoreCase("URL")) {
            webDriver.get(properties.getProperty(value));
            Thread.sleep(5000);
        }
    }

    public void click(Properties properties, String operation, String objectName, String objectType) throws Exception {
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        if(operation.equalsIgnoreCase("Click")) {
            webDriver.findElement(this.getObject(properties, objectName, objectType)).click();
            Thread.sleep(5000);
        }
    }

    public void capture(Properties properties, String operation, String objectName, String objectType, String value) throws Exception {
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        if(operation.equalsIgnoreCase("Capture")) {
            webDriver.findElement(this.getObject(properties, objectName, objectType)).sendKeys(value);
            Thread.sleep(5000);
        }
    }

    public void getText(Properties properties, String operation, String objectName, String objectType) throws Exception {
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        if(operation.equalsIgnoreCase("Get")) {
            String errorMessage = webDriver.findElement(this.getObject(properties, objectName, objectType)).getText();
            String expectedMessage = "You need to upload at least one file.";

            Assert.assertEquals(errorMessage, expectedMessage);
            Thread.sleep(5000);
        }
    }

    /**
     *
     * @param properties
     * @param objectName
     * @param objectType
     * @return
     * @throws Exception
     */
    private By getObject(Properties properties, String objectName, String objectType) throws Exception {
        if(objectType.equalsIgnoreCase("Id")) {
            return By.id(properties.getProperty(objectName));
        }
        else if(objectType.equalsIgnoreCase("Xpath")) {
            return By.xpath(properties.getProperty(objectName));
        }
        else if(objectType.equalsIgnoreCase("ClassName")) {
            return By.className(properties.getProperty(objectName));
        }
        else if(objectType.equalsIgnoreCase("Name")) {
            return By.name(properties.getProperty(objectName));
        }
        else if(objectType.equalsIgnoreCase("CSS")) {
            return By.cssSelector(properties.getProperty(objectName));
        }
        else if(objectType.equalsIgnoreCase("Link")) {
            return By.linkText(properties.getProperty(objectName));
        }
        else if(objectType.equalsIgnoreCase("PartialLink")) {
            return By.partialLinkText(properties.getProperty(objectName));
        }
        else {
            throw new Exception("Not Found");
        }
    }
}
